package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String lid;
    private String password;
    private String name;
    private int branch;
    private int departmentmanager;
    private String user;
    private int registeredperson;
    private String text;
    private Date created_date;
    private int post_id;
    
    // getter setter は省略
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public int getDepartmentmanager() {
		return departmentmanager;
	}
	public void setDepartmentmanager(int departmentmanager) {
		this.departmentmanager = departmentmanager;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public String getLid() {
		return lid;
	}
	public void setLid(String lid) {
		this.lid = lid;
	}
	public int getRegisteredperson() {
		return registeredperson;
	}
	public void setRegisteredperson(int registeredperson) {
		this.registeredperson = registeredperson;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
}
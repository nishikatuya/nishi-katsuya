package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Post;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("registeredperson");
            sql.append(", subject");
            sql.append(", category");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", delete_flag");
            sql.append(") VALUES (");
            sql.append(" ?"); // registeredperson
            sql.append(", ?"); // subject
            sql.append(", ?"); //category
            sql.append(", ?");
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", 0");
            sql.append(")");
            
            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getRegisteredperson());
            ps.setString(2, post.getSubject());
            ps.setString(3, post.getCategory());
            ps.setString(4, post.getText());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE messages SET");
            sql.append(" delete_flag = 1");
            sql.append(" WHERE");
            sql.append(" messages.id = ?");
            
            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}

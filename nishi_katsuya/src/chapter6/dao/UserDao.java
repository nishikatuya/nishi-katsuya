package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("  id");
            sql.append(", lid");
            sql.append(",  password");
            sql.append(", name");
            sql.append(", branch");
            sql.append(", departmentmanager");
            sql.append(", status");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(", ?"); 
            sql.append(", ?"); 
            sql.append(", ?"); 
            sql.append(", ?"); 
            sql.append(", ?"); 
            sql.append(", ?");
            sql.append(", CURRENT_TIMESTAMP"); 
            sql.append(", CURRENT_TIMESTAMP"); 
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, user.getId());
            ps.setString(2, user.getLid());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getName());
            ps.setInt(5, user.getBranch());
            ps.setInt(6, user.getDepartmentmanager());
            ps.setInt(7, user.getStatus());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


public User getUser(Connection connection, String lid,
        String password) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users WHERE (lid = ?) AND password = ?";

        ps = connection.prepareStatement(sql);
        ps.setString(1, lid);
        ps.setString(2, password);

        ResultSet rs = ps.executeQuery();
        List<User> userList = toUserList(rs);
        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

private List<User> toUserList(ResultSet rs) throws SQLException {

    List<User> ret = new ArrayList<User>();
    try {
        while (rs.next()) {
        	int id = rs.getInt("id");
        	String lid = rs.getString("lid");
            String password = rs.getString("password");
            String name = rs.getString("name");
            int branch = rs.getInt("branch");
            int departmentmanager = rs.getInt("departmentmanager");
            int delete_flag = rs.getInt("delete_flag");
            int status = rs.getInt("status");
            Timestamp createdDate = rs.getTimestamp("created_date");
            Timestamp updatedDate = rs.getTimestamp("updated_date");

            User user = new User();
            user.setId(id);
            user.setLid(lid);
            user.setPassword(password);
            user.setName(name);
            user.setBranch(branch);
            user.setDepartmentmanager(departmentmanager);
            user.setDelete_flag(delete_flag);
            user.setStatus(status);
            user.setCreatedDate(createdDate);
            user.setUpdatedDate(updatedDate);

            ret.add(user);
        }
        return ret;
    } finally {
        close(rs);
    }
}

public User getUser(Connection connection, int id) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users WHERE id = ?";

        ps = connection.prepareStatement(sql);
        ps.setInt(1, id);

        ResultSet rs = ps.executeQuery();
        List<User> userList = toUserList(rs);
        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

public void update(Connection connection, User user) {

    PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE users SET");
        sql.append("  lid = ?");
        sql.append(",  password = ?");
        sql.append(", name = ?");
        sql.append(", branch = ?");
        sql.append(", departmentmanager = ?");
//      sql.append(", status = ?");
//      sql.append(", updated_date = CURRENT_TIMESTAMP");
        sql.append(" WHERE");
        sql.append(" id = ?");

        ps = connection.prepareStatement(sql.toString());

        ps.setString(1, user.getLid());
        ps.setString(2, user.getPassword());
        ps.setString(3, user.getName());
        ps.setInt(4, user.getBranch());
        ps.setInt(5, user.getDepartmentmanager());
        ps.setInt(6, user.getId());
//      ps.setInt(6, user.getStatus());
        System.out.println(ps.toString());
        int count = ps.executeUpdate();
        System.out.println(count);
        if (count == 0) {
            throw new NoRowsUpdatedRuntimeException();
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }

}

public List<User> getAllUser(Connection connection) {

    PreparedStatement ps = null;
    try {
    	
    	StringBuilder sql = new StringBuilder();
    	 sql.append("SELECT ");
    	 sql.append("users.id as id, ");
		 sql.append("users.lid as lid, ");
		 sql.append("users.password as password, ");
		 sql.append("users.name as name, ");
		 sql.append("users.branch as branch, ");
		 sql.append("users.departmentmanager as departmentmanager, ");
		 sql.append("users.status as status, ");
		 sql.append("users.created_date as created_date, ");
		 sql.append("users.updated_date as updated_date, ");
		 sql.append("users.delete_flag as delete_flag, ");
		 sql.append("branches.branch_name as branch_name, ");
		 sql.append("departmentmanagers.departmentmanager_name as departmentmanager_name ");
		 sql.append("FROM users ");
		 sql.append("INNER JOIN branches ");
		 sql.append("ON users.branch = branches.id ");
		 sql.append("INNER JOIN departmentmanagers ");
		 sql.append("ON users.departmentmanager = departmentmanagers.id ");
		
		 ps = connection.prepareStatement(sql.toString());
		 
        ResultSet rs = ps.executeQuery();
        List<User> allUserList = toAllUserList(rs);
        if (allUserList.isEmpty() == true) {
            return null;
        
        } else {
            return allUserList;
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

private List<User> toAllUserList(ResultSet rs) throws SQLException {

    List<User> ret = new ArrayList<User>();
    try {
        while (rs.next()) {
        	int id = rs.getInt("id");
        	String lid = rs.getString("lid");
            String password = rs.getString("password");
            String name = rs.getString("name");
            int branch = rs.getInt("branch");
            int departmentmanager = rs.getInt("departmentmanager");
            int status = rs.getInt("status");
            String branch_Name = rs.getString("branch_name");
            String departmentmanager_Name = rs.getString("departmentmanager_name");
            int delete_flag=rs.getInt("delete_flag");
            Timestamp createdDate = rs.getTimestamp("created_date");
            Timestamp updatedDate = rs.getTimestamp("updated_date");

            User user = new User();
            user.setId(id);
            user.setLid(lid);
            user.setPassword(password);
            user.setName(name);
            user.setBranch(branch);
            user.setDepartmentmanager(departmentmanager);
            user.setDelete_flag(delete_flag);
            user.setStatus(status);
            user.setCreatedDate(createdDate);
            user.setUpdatedDate(updatedDate);
            user.setBranch_name(branch_Name);
            user.setDepartmentmanager_name(departmentmanager_Name);
            
            ret.add(user);
        }
        return ret;
    } finally {
        close(rs);
    }
}

public void stop(Connection connection, User user) {

    PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE users SET");
        sql.append(" delete_flag = 1");
        sql.append(" WHERE");
        sql.append(" users.id = ?");
        
        ps = connection.prepareStatement(sql.toString());

        ps.setInt(1, user.getId());

        int count = ps.executeUpdate();
        if (count == 0) {
            throw new NoRowsUpdatedRuntimeException();
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

public void revival(Connection connection, User user) {

    PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE users SET");
        sql.append(" delete_flag = 0");
        sql.append(" WHERE");
        sql.append(" users.id = ?");
        
        ps = connection.prepareStatement(sql.toString());

        ps.setInt(1, user.getId());

        int count = ps.executeUpdate();
        if (count == 0) {
            throw new NoRowsUpdatedRuntimeException();
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}
}
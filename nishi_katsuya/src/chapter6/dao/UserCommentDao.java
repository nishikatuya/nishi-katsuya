package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.created_date as created_date, ");
            sql.append("comments.registeredperson as registeredperson, ");
            sql.append("comments.post_id as post_id, ");
            sql.append("users.name as name ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.registeredperson = users.id ");
            sql.append("INNER JOIN messages ");
            sql.append("ON comments.post_id = messages.id ");
            sql.append("WHERE ");
            sql.append("comments.delete_flag = 0 ");
            sql.append("ORDER BY created_date DESC limit " + num);;
            
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int registeredperson = rs.getInt("registeredperson");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int post_id = rs.getInt("post_id");

                UserComment comment = new UserComment();
                comment.setId(id);
                comment.setName(name);
                comment.setRegisteredperson(registeredperson);
                comment.setText(text);
                comment.setCreated_date(createdDate);
                comment.setPost_id(post_id);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
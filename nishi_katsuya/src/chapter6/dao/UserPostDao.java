package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserPost;
import chapter6.exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.registeredperson as registeredperson, ");
            sql.append("messages.category as category, ");
            sql.append("messages.created_date as created_date, ");
            sql.append("users.name as name ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.registeredperson = users.id ");
            sql.append("WHERE ");
            sql.append("messages.delete_flag = 0 ");
            sql.append("ORDER BY created_date DESC limit " + num);
            
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int registeredperson = rs.getInt("registeredperson");
                String subject = rs.getString("subject");
                String category = rs.getString("category");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");
                
                UserPost post = new UserPost();
                post.setId(id);
                post.setName(name);
                post.setRegisteredperson(registeredperson);
                post.setText(text);
                post.setSubject(subject);
                post.setCategory(category);
                post.setCreated_date(createdDate);
                
                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}

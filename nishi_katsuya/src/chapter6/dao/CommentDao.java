package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Comment;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("text");
            sql.append(", registeredperson");
            sql.append(", post_id");
            sql.append(", created_date");
            sql.append(", delete_flag");
            sql.append(") VALUES (");
            sql.append(" ?"); 
            sql.append(", ?"); 
            sql.append(", ?"); 
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", 0");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getText());
            ps.setInt(2, comment.getRegisteredperson());
            ps.setInt(3, comment.getPost_id());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE comments SET");
            sql.append(" delete_flag = 1");
            sql.append(" WHERE");
            sql.append(" comments.id = ?");
            
            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}
package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentdeleteService;

@WebServlet(urlPatterns = { "/Commentdelete" })
public class CommentdeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
    HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();

        User user = (User) session.getAttribute("loginUser");
        request.setAttribute("loginUser" , user);

        Comment update = new Comment();
        update.setId(Integer.parseInt(request.getParameter("id")));
//      update.setId(user.getId());

        new CommentdeleteService().update(update);

        response.sendRedirect("./");
  }
}

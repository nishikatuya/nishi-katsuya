package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;


 @WebServlet(urlPatterns = { "/useredit" })
        public class UserEditServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

 @Override
        protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
	 
        int id = Integer.parseInt(request.getParameter("id"));
        
        System.out.println(id);
        
        User editUser = new UserService().getUser(id);

        request.setAttribute("editUser", editUser);
        request.getRequestDispatcher("useredit.jsp").forward(request, response);
  }
 
 @Override
    	protected void doPost(HttpServletRequest request,
    	HttpServletResponse response) throws ServletException, IOException {

    	List<String> messages = new ArrayList<String>();
    	HttpSession session = request.getSession();
    	User editUser = getEditUser(request);

    	if (isValid(request, messages) == true) {

    	try {
    	new UserService().update(editUser);
    	
      } catch (NoRowsUpdatedRuntimeException e) {
    	  
    	request.setAttribute("editUser", editUser);
    	request.getRequestDispatcher("useredit.jsp").forward(request, response);
    	return;
     }

    	session.setAttribute("loginUser", editUser);

        response.sendRedirect("./");
    	
    	} else {
        session.setAttribute("errorMessages", messages);
    	request.setAttribute("editUser", editUser);
        request.getRequestDispatcher("settings.jsp").forward(request, response);
    	}
    }

    	private User getEditUser(HttpServletRequest request)
    	throws IOException, ServletException {

    		User editUser = new User();
    		editUser.setLid(request.getParameter("lid"));
    		editUser.setPassword(request.getParameter("password"));
    		editUser.setName(request.getParameter("name"));
    		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
    		editUser.setDepartmentmanager(Integer.parseInt(request.getParameter("departmentmanager")));
    		editUser.setId(Integer.parseInt(request.getParameter("id")));
    		
    		return editUser;
    	}

    	private boolean isValid(HttpServletRequest request, List<String> messages) {

    		String lid = request.getParameter("lid");
    		String password = request.getParameter("password");

    		if (StringUtils.isEmpty(lid) == true) {
    			messages.add("ログインIDを入力してください");
    		}
    		if (StringUtils.isEmpty(password) == true) {
    			messages.add("パスワードを入力してください");
    		}
    		
    		if (messages.size() == 0) {
    			return true;
    		} else {
    			return false;
    		}
    	}
 }

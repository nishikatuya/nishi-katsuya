package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.service.UserstopService;

@WebServlet(urlPatterns = { "/userstop" })
public class UserstopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

    	User stop = new User();

    	stop.setId(Integer.parseInt(request.getParameter("id")));
    	
    	new UserstopService().stop(stop);

        response.sendRedirect("settings");
    }
}

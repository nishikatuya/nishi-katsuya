package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Post;
import chapter6.beans.User;
import chapter6.service.PostService;

@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
           
    HttpServletResponse response) throws ServletException, IOException {
    	 
    request.getRequestDispatcher("post.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Post post = new Post();
            post.setText(request.getParameter("text"));
            post.setRegisteredperson(user.getId());
            post.setSubject(request.getParameter("subject"));
            post.setCategory(request.getParameter("category"));            

            new PostService().register(post);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("/post.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String subject = request.getParameter("subject");
    	String category = request.getParameter("category");
        String text = request.getParameter("text");
        
        if (StringUtils.isEmpty(text) == true) {
            messages.add("メッセージを入力してください");
        }
        if (30 < subject.length()) {
            messages.add("件名は30文字以下で入力してください");
        }
        if (10 < category.length()) {
            messages.add("カテゴリーは10文字以下で入力してください");
        }
        if (1000 < text.length()) {
            messages.add("本文は1000文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}

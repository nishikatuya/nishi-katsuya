package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.UserComment;
import chapter6.beans.UserPost;
import chapter6.service.CommentService;
import chapter6.service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

//        User user = (User) request.getSession().getAttribute("loginUser");
//        boolean isShowMessageForm;
//        if (user != null) {
//            isShowMessageForm = true;
//        } else {
//            isShowMessageForm = false;
//        }

        List<UserPost> messages = new PostService().getMessages();
        request.setAttribute("post", messages);
        
        List<UserComment> comments = new CommentService().getComments();
        request.setAttribute("comment", comments);
//        request.setAttribute("isShowMessageForm", isShowMessageForm);


        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}

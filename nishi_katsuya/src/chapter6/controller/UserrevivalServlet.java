package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.service.UserrevivalService;

@WebServlet(urlPatterns = { "/userrevival" })
public class UserrevivalServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

    	User revival = new User();

    	revival.setId(Integer.parseInt(request.getParameter("id")));
    	
    	new UserrevivalService().revival(revival);

        response.sendRedirect("settings");
    }
}

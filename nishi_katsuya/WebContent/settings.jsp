<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー</title>
</head>
<body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

                <table border="1">
                <tr>
                <th>名前</th><th>支店</th><th>部署・役職</th><th>停止</th><th>編集</th>
                </tr>
                <c:forEach items="${allUserList}" var="list">
                <tr>
                <td><c:out value="${list.name }"/></td>
                <td><c:out value="${list.branch_name }"/></td>
                <td><c:out value="${list.departmentmanager_name }"/></td>
                <td>
                <c:if test="${list.delete_flag == 0}">
                <form action="userstop" method="post">
			    <input type="hidden" name="id" value="${list.id}">
		        <button type="submit" >停止</button></form>
		        </c:if>
		        <c:if test="${list.delete_flag == 1}">
		        <form action="userrevival" method="post">
		        <input type="hidden" name="id" value="${list.id}">
	            <button type="submit" >復活</button>
	            </form>
	            </c:if>
		        </td> 
                <td><a href="useredit?id=${list.id }">編集</a></td> 
                </tr>
                
                </c:forEach>
                </table>
                <a href="./">戻る</a>
            
            <div class="copyright"> Copyright(c)Your Name</div>
        </div>
    </body>
</html>

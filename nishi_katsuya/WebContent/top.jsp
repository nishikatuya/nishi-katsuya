<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<a href="login">ログイン</a> <a href="signup">登録する</a>
		</div>
		<div class="copyright">Copyright(c)YourName</div>
	</div>
</body>
</html>

<div class="header">
	<c:if test="${ empty loginUser }">
		<a href="login">ログイン</a>
		<a href="signup">登録する</a>
	</c:if>
	<c:if test="${ not empty loginUser }">
		<a href="./">ホーム</a>
		<a href="newPost">新規投稿</a>
		<a href="settings">ユーザー管理画面</a>
		<a href="logout">ログアウト</a>
	</c:if>
</div>

<c:if test="${ not empty loginUser }">
	<div class="profile">
		<div class="name">
			<h2>
				<c:out value="${loginUser.name}" />
			</h2>
		</div>
		<div class="lid">
			@<c:out value="${loginUser.lid}" />
		</div>
	</div>

	<c:forEach items="${post}" var="post">
	<form action="newPostdelete" method="post">
		<div class="post">
		
			<div class="lid-name">
				<span class="lid"><c:out value="${post.lid}" /></span>
				 <span class="name"><c:out value="${post.name}" /></span>
			
			</div>
			<div class="subject">
				件名：
				<c:out value="${post.subject}" />
			</div>
			<div class="category">
		        カテゴリー：
		        <c:out value="${post.category}" />
			</div>
			<div class="text">
			    本文：
				<c:out value="${post.text}" />
			</div>
			<div class="date">
				<fmt:formatDate value="${post.created_date}"
					pattern="yyyy/MM/dd HH:mm:ss" /></div>
					
					<input type = "submit" value="投稿削除">
					 <input type="hidden" name="id" value="${post.id}">
					  </div>
	</form><br />
					
					コメント:<br />
					
			<div class="form-comment">
			
					<form action="newComment" method="post">
						<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
						<br /> <input type="submit" value="コメント"> <input
							type="hidden" name="post_id" value="${post.id}">
					</form>
					
				</div>
				
			<div class="comments">
			<c:if test="${ not empty errorComments }">
                <div class="errorComments">
                    <ul>
                        <c:forEach items="${errorComments}" var="comment">
                            <li><c:out value="${comment}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorComments" scope="session" />
            </c:if>
					<c:forEach items="${comment}" var="comment">
					
					<form action="Commentdelete" method="post">
					<c:if test="${post.id == comment.post_id}">
					<div class="text">
							コメント：
							<c:out value="${comment.text}" />
						</div>
						<div class="date">
							<fmt:formatDate value="${comment.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
							<span class="name"><c:out value="${comment.name}" /></span>
							<input type = "submit" value="コメント削除">
                            <input type="hidden" name="id" value="${comment.id}">
						</div>
						</c:if>
						</form>
					</c:forEach>
	            </div><br>
    </c:forEach>
</c:if>